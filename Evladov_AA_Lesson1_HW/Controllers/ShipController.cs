﻿using Evladov_AA_Lesson1_HW.Interfaces;
using Evladov_AA_Lesson1_HW.Models;
using Evladov_AA_Lesson1_HW.Views;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Evladov_AA_Lesson1_HW.Controllers
{
    class ShipController: BaseObjectController
    {
        public event Message MessageDie;
        Point LimitDir = new Point(20, 20);
        DateTime lastShotTime = DateTime.Now;
        new ShipModel _model;

        public ShipController(ShipModel model) : base()
        {
            _model = model;
            MessageDie += Game.getInstance().Finish;
            Game.getInstance()._form.KeyDown += Ship_KeyDownPress;
            Game.getInstance()._form.KeyUp += Ship_KeyUpPress;
        }

        public override void SelfDestroy()
        {
            Game.getInstance().ControllersUpdate -= Update;
            Game.getInstance()._form.KeyDown -= Ship_KeyDownPress;
            Game.getInstance()._form.KeyUp -= Ship_KeyUpPress;
            _model = null;
        }

        public void Die()
        {
            MessageDie?.Invoke();
        }

        private void Ship_KeyUpPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                if (_model.Dir.Y < 0)
                    _model.Dir.Y = 0;
            }
            if (e.KeyCode == Keys.Down)
            {
                if (_model.Dir.Y > 0)
                    _model.Dir.Y = 0;
            }
            if (e.KeyCode == Keys.Left)
            {
                if (_model.Dir.X < 0)
                    _model.Dir.X = 0;
            }
            if (e.KeyCode == Keys.Right)
            {
                if (_model.Dir.X > 0)
                    _model.Dir.X = 0;
 
            }
        }

        void Ship_KeyDownPress(object sender, KeyEventArgs e)
        {

            //Управление кораблем
            if (e.KeyCode == Keys.Up)
            {
                if (_model.Dir.Y < -LimitDir.Y)
                    _model.Dir.Y = -LimitDir.Y;
                else
                    _model.Dir.Y -= 5;
            }
            if (e.KeyCode == Keys.Down)
            {
                if (_model.Dir.Y > LimitDir.Y)
                    _model.Dir.Y = LimitDir.Y;
                else
                    _model.Dir.Y += 5;
            }
            if (e.KeyCode == Keys.Left)
            {
                if (_model.Dir.X < -LimitDir.X)
                    _model.Dir.X = -LimitDir.X;
                else
                    _model.Dir.X -= 5;
            }
            if (e.KeyCode == Keys.Right)
            {
                if (_model.Dir.X > LimitDir.X)
                    _model.Dir.X = LimitDir.X;
                else
                    _model.Dir.X += 5;
            }

            //Управление стрельбой
            if (e.KeyCode == Keys.Space)
            {
                //Стрелят ьразрешено раз в 100 мс
                if (DateTime.Now > lastShotTime.AddSeconds(0.1f))
                {
                    lastShotTime = DateTime.Now;
                    BulletModel _bullet = new BulletModel(new Point(_model.Pos.X + _model.Size.Width, _model.Pos.Y + (_model.Size.Height / 2)), new Point(10, 0), new Size(4, 1));
                    new BulletController(_bullet);
                    new BulletView(_bullet);
                }
            }


        }

        protected override void Update()
        {
            _model.Pos.X = _model.Pos.X + _model.Dir.X;
            _model.Pos.Y = _model.Pos.Y + _model.Dir.Y;

            if (_model.Pos.X > Game.getInstance().Width / 2)
            {
                _model.Pos.X = Game.getInstance().Width / 2;
            }

            if (_model.Pos.X < 0)
            {
                _model.Pos.X = 0;
            }

            if (_model.Pos.Y > Game.getInstance().Height-_model.Size.Height)
            {
                _model.Pos.Y = Game.getInstance().Height-_model.Size.Height;
            }
            if (_model.Pos.Y < 0)
            {
                _model.Pos.Y = 0;
            }



            
            foreach (BaseObjectController a in Game.getInstance()._controllers.FindAll(a => a._model is IReaction))
            {
                if (_model.Collision(a._model))
                {
                    a.Regenerate(Game.getInstance().Width, Game.getInstance().Height);
                    (a._model as IReaction).Reaction(_model);
                }
            }

            if(_model.Health <= 0)
            {
                Die();
                Console.WriteLine("Игра окончена, корабль - уничтожен!");
            }

            

        }
    }
}
