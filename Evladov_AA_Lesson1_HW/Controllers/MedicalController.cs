﻿using System;


namespace Evladov_AA_Lesson1_HW.Controllers
{
    class MedicalController: BaseObjectController
    {
        public MedicalController(MedicalModel model) : base()
        {
            _model = model;
        }

        protected override void Update()
        {
            _model.Pos.X = _model.Pos.X + _model.Dir.X;
            if (_model.Pos.X + _model.Size.Width < 0)
            {
                _model.Pos.X = new Random().Next(Game.getInstance().Width, Game.getInstance().Width*2) + _model.Size.Width;
                _model.Pos.Y = new Random().Next(0, Game.getInstance().Height);
            }
        }
    }
}
