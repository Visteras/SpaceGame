﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evladov_AA_Lesson1_HW.Controllers
{
    class BulletController: BaseObjectController
    {

        public new BulletModel _model;
        int level;

        public BulletController(BulletModel model) : base()
        {
            _model = model;
            level = Game.getInstance().level;
        }

        protected override void Update()
        {
            if (level == Game.getInstance().level)
            {
                if (_model.State == BulletModel.AmmoState.Space)
                    _model.Pos.X = _model.Pos.X + _model.Dir.X;

                // Получаем Все астероиды и проверяем на столкновение
                //TODO Можно переделать на работу через реакцию на столкновение с некоторыми объектами
                foreach (BaseObjectController a in Game.getInstance()._controllers.FindAll(a => a._model is AsteroidModel))
                {
                    if (_model.Collision(a._model) && _model.State == BulletModel.AmmoState.Space)
                    {

                        Game.getInstance().destroyedAsteroids++;
                        logger.Log($"Количество сбитых астероидов: {Game.getInstance().destroyedAsteroids}");
                        _model.State = BulletModel.AmmoState.Used;
                        a.Regenerate(Game.getInstance().Width, Game.getInstance().Height);
                        Game.getInstance().ControllersUpdate -= Update;
                        System.Media.SystemSounds.Hand.Play();
                    }
                }
            } else
            {
                SelfDestroy();
            }

        }

    }
}
