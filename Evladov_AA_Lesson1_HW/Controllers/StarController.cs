﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evladov_AA_Lesson1_HW.Controllers
{
    class StarController: BaseObjectController
    {
        public StarController(StarModel model): base()
        {
            _model = model;
        }

        protected override void Update()
        {
            _model.Pos.X = _model.Pos.X + _model.Dir.X;
            if (_model.Pos.X + _model.Size.Width < 0) _model.Pos.X = Game.getInstance().Width + _model.Size.Width;
        }
    }
}
