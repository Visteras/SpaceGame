﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Evladov_AA_Lesson1_HW.Interfaces;
using Evladov_AA_Lesson1_HW.Models;
using GeekBrains.CSharpSecond.SpaceGame;
using GeekBrains.CSharpSecond.SpaceGame.Utils;
using static Evladov_AA_Lesson1_HW.Game;

namespace Evladov_AA_Lesson1_HW.Controllers
{
    public abstract class BaseObjectController
    {
        public BaseObjectModel _model;
        protected Logger logger;
        public delegate void Message();

        protected abstract void Update();

        protected BaseObjectController()
        {
            Game.getInstance().ControllersUpdate += Update;
            logger = new Logger(this.GetType());
        }
        public virtual void SelfDestroy()
        {
            Game.getInstance().ControllersUpdate -= Update;
            _model = null;
        }

        internal void Regenerate(int width, int height)
        {
            _model.Pos.X = width;
            if(height != 0)
            {
                _model.Pos.Y = new Random().Next(0, height);
            }
        }
    }

}
