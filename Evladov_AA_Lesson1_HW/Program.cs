﻿using System.Windows.Forms;

namespace Evladov_AA_Lesson1_HW
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Form form = new Form();
            form.Width = 800;
            form.Height = 600;
            Game.getInstance().Init(form);
            form.Show();
            Application.Run(form);
        }
    }
}