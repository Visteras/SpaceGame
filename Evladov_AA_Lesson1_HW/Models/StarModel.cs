﻿using Evladov_AA_Lesson1_HW.Models;
using System;
using System.Drawing;
using System.IO;
using System.Threading;

namespace Evladov_AA_Lesson1_HW
{
    public class StarModel : BaseObjectModel
    {

        public StarModel(Point pos, Size size): base(pos)
        {
            Pos = pos;
            
            if(size.Width != size.Height || size.Height < 0 || size.Width < 0 )
            {
                throw new GameObjectException();
            }
            Size = size;

            //Чем больше тем быстрее, чем дальше - тем соответственно медленне будет двигаться
            Dir.X = -Size.Width/2;
        }

    }
}