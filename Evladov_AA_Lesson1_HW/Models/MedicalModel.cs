﻿using Evladov_AA_Lesson1_HW.Interfaces;
using Evladov_AA_Lesson1_HW.Models;
using GeekBrains.CSharpSecond.SpaceGame.Utils;
using System;
using System.Drawing;
using System.IO;
using System.Threading;

namespace Evladov_AA_Lesson1_HW
{
    public class MedicalModel : BaseObjectModel, IReaction
    {
        public MedicalModel(Point pos, Size size): base(pos)
        {
            Pos = pos;
            
            Size = size;

            Dir.X = -Size.Width/2;
            logger = new Logger(this.GetType());
        }

        public void Reaction(ICollision obj)
        {
            
            if(obj is ShipModel)
            {
                //Если столкновение с кораблем - лечим
                (obj as ShipModel).Health = (obj as ShipModel).Health + 25;
                logger.Log("(АПТЕЧКА)Здоровья у корабля:" + (obj as ShipModel).Health);
                //LogMsg?.Invoke("(АПТЕЧКА)Здоровья у корабля:" + (obj as ShipModel).Health);

            }
        }
    }
}