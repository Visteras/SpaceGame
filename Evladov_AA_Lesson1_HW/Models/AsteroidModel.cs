﻿using Evladov_AA_Lesson1_HW.Interfaces;
using Evladov_AA_Lesson1_HW.Models;
using System;
using System.Drawing;


namespace Evladov_AA_Lesson1_HW
{
    public class AsteroidModel : BaseObjectModel, IReaction
    {

        public AsteroidModel(Point pos, Size size): base(pos)
        {
            Pos = pos;
            
            if(size.Width != size.Height || size.Height < 0 || size.Width < 0 )
            {
                throw new GameObjectException();
            }
            Size = size;
            Size.Height *= 8;
            Size.Width *= 8;

            //Чем больше тем быстрее, чем дальше - тем соответственно медленне будет двигаться
            Dir.X = -Size.Width/2;

        }

        public void Reaction(ICollision obj)
        {
            if(obj is ShipModel)
            {
                //Если столкновение с кораблем - наносим урон
                (obj as ShipModel).Health = (obj as ShipModel).Health - 5;
                logger.Log("(АСТЕРОИД)Здоровья у корабля:" + (obj as ShipModel).Health);
            }
        }
    }
}