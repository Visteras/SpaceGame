﻿using Evladov_AA_Lesson1_HW.Controllers;
using Evladov_AA_Lesson1_HW.Interfaces;
using GeekBrains.CSharpSecond.SpaceGame;
using GeekBrains.CSharpSecond.SpaceGame.Utils;
using System;
using System.Drawing;

namespace Evladov_AA_Lesson1_HW.Models
{
    class GameObjectException : Exception { }

    public abstract class BaseObjectModel: ICollision
    {
        protected Action<string> LogMsg;
        public Point Pos;
        public Point Dir;
        public Size Size;
        protected Logger logger;

        public Rectangle Rect => new Rectangle(Pos, Size);

        public bool Collision(ICollision obj) => obj.Rect.IntersectsWith(this.Rect);

        public BaseObjectModel(Point pos, Point dir, Size size)
        {
            Pos = pos;
            Dir = dir;
            Size = size;
            LogMsg += Game.getInstance().Log;
            logger = new Logger(this.GetType());
        }

        private void ModelLog(string msg)
        {

            Game.getInstance().Log($"MODEL\t{GetType().Name}\t{msg}");
        }

        public BaseObjectModel(Point pos, Point dir): this(pos, dir, new Size(0,0))
        {
        }

        public BaseObjectModel(Point pos): this(pos, new Point())
        {
        }

        public BaseObjectModel(): this(new Point(0,0))
        {
        } 

    }
}