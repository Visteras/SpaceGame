﻿using Evladov_AA_Lesson1_HW.Interfaces;
using Evladov_AA_Lesson1_HW.Models;
using System;
using System.Drawing;
using System.IO;
using System.Threading;

namespace Evladov_AA_Lesson1_HW
{
    public class ShipModel : BaseObjectModel
    {
        const int maxHealth = 100;
        int health;
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                health = value;
                if (health > maxHealth) { health = maxHealth; }
            }
        }

        public ShipModel(Point pos, Point dir, Size size): base(pos, dir, size)
        {
            Pos = pos;
            Size = size;
            Dir = dir;
            Health = maxHealth;
        }

    }
}