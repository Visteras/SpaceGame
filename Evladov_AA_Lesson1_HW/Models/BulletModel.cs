﻿using Evladov_AA_Lesson1_HW.Models;
using System;
using System.Drawing;
using System.IO;
using System.Threading;

namespace Evladov_AA_Lesson1_HW
{

    public class BulletModel : BaseObjectModel
    {
        public AmmoState State { get; set; } = AmmoState.Cage;

        public enum AmmoState
        {
            Cage,
            Space,
            Used
        }

        public BulletModel(Point pos, Point dir, Size size): base(pos, dir, size)
        {
            State = AmmoState.Space;
            Pos = pos;
            Size = size;
            Dir = dir;
        }



    }
}