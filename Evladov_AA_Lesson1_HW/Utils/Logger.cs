﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekBrains.CSharpSecond.SpaceGame.Utils
{
    public class Logger
    {
        Type type;

        public Logger(Type type)
        {
            this.type = type;
        }

        public void Log(string msg)
        {
            Console.WriteLine($"{type.Name}\t{msg}");
        }
    }
}
