﻿using GeekBrains.CSharpSecond.SpaceGame;
using System;
using System.Drawing;

namespace Evladov_AA_Lesson1_HW.Views
{
    class AsteroidView: BaseObjectView
    {
        public AsteroidView(AsteroidModel model): base()
        {
            _model = model;
        }

        public override void Draw()
        {
            Game.getInstance().Buffer.Graphics.DrawImage(Resources.Asteroid, _model.Rect);
            //Проверка столкновений 
            //Game.getInstance().Buffer.Graphics.DrawRectangle(new Pen(Color.Wheat), _model.Rect);
        }
    }
}
