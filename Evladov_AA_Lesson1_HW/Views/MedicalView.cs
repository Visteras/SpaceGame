﻿using GeekBrains.CSharpSecond.SpaceGame;


namespace Evladov_AA_Lesson1_HW.Views
{
    class MedicalView: BaseObjectView
    {
        public MedicalView(MedicalModel model): base()
        {
            _model = model;
        }

        public override void Draw()
        {
            Game.getInstance().Buffer.Graphics.DrawImage(Resources.medical_kit_1, _model.Rect);
            //Проверка столкновений 
            //Game.getInstance().Buffer.Graphics.DrawRectangle(new Pen(Color.Wheat), _model.Rect);
        }
    }
}
