﻿using System;
using System.Drawing;

namespace Evladov_AA_Lesson1_HW.Views
{
    class StarView: BaseObjectView
    {
        public StarView(StarModel model) : base()
        {
            _model = model;
        }

        public override void Draw()
        {
            PointF[] pts = StarPoints(5, _model.Rect);
            Game.getInstance().Buffer.Graphics.DrawPolygon(Pens.White, pts);
        }


        //Метод позволяющий получить точки по которым можно нарисовать именно звезду
        private PointF[] StarPoints(int num_points, Rectangle bounds)
        {
            PointF[] pts = new PointF[num_points];

            double rx = bounds.Width / 2;
            double ry = bounds.Height / 2;
            double cx = bounds.X + rx;
            double cy = bounds.Y + ry;

            double theta = -Math.PI / 2;
            double dtheta = 4 * Math.PI / num_points;
            for (int i = 0; i < num_points; i++)
            {
                pts[i] = new PointF(
                    (float)(cx + rx * Math.Cos(theta)),
                    (float)(cy + ry * Math.Sin(theta)));
                theta += dtheta;
            }

            return pts;
        }
    }
}
