﻿using GeekBrains.CSharpSecond.SpaceGame;
using System;
using System.Drawing;

namespace Evladov_AA_Lesson1_HW.Views
{
    class ShipView: BaseObjectView
    {
        public ShipView(ShipModel model): base()
        {
            _model = model;
        }

        public override void Draw()
        {
            Game.getInstance().Buffer.Graphics.DrawImage(Resources.Spaceship, _model.Rect);
        }
    }
}
