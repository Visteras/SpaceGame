﻿using System;
using System.Drawing;

namespace Evladov_AA_Lesson1_HW.Views
{
    class BulletView: BaseObjectView
    {
        public new BulletModel _model;
        int level;

        public BulletView(BulletModel model): base()
        {
            _model = model;
            level = Game.getInstance().level;
        }

        public override void Draw()
        {
            if (level == Game.getInstance().level)
            {
                if (_model.State == BulletModel.AmmoState.Used)
                    Game.getInstance().ViewsUpdate -= Draw;

                if (_model.State == BulletModel.AmmoState.Space)
                    Game.getInstance().Buffer.Graphics.DrawRectangle(Pens.OrangeRed, _model.Rect);
            } else
            {
                SelfDestroy();
            }
        }
    }
}
