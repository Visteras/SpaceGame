﻿using Evladov_AA_Lesson1_HW.Models;
using GeekBrains.CSharpSecond.SpaceGame;

namespace Evladov_AA_Lesson1_HW.Views
{
    public abstract class BaseObjectView
    {
        public BaseObjectModel _model;
        public abstract void Draw();
        protected BaseObjectView()
        {
            Game.getInstance().ViewsUpdate += Draw;
        }
        public void SelfDestroy()
        {
            Game.getInstance().ViewsUpdate -= Draw;
            _model = null;
        }
    }

}
