﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Evladov_AA_Lesson1_HW.Controllers;
using Evladov_AA_Lesson1_HW.Views;
using System.Collections.Generic;
using System.Threading;
using GeekBrains.CSharpSecond.SpaceGame;
using Evladov_AA_Lesson1_HW.Models;

namespace Evladov_AA_Lesson1_HW
{
    class Game
    {
        private static Game instance;
        private static object syncRoot = new Object();
        public event Action ControllersUpdate;
        public event Action ViewsUpdate;
        public Form _form;
        public int countAsteroid = 20;
        public int destroyedAsteroids = 0;
        public int level = 1;

        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer { Interval = 100 };


        //Пусть будут еще и модели, раздумываю над тем что бы убрать отсюда ссылки на контроллеры и вьюхи, мы всё же работаем толкьо с моделями, здесь контроллеры и вьюхи не обязательны... вроде... 
        public List<BaseObjectModel> _models;
        public List<BaseObjectView> _views;
        public List<BaseObjectController> _controllers;
        

        public BufferedGraphics Buffer;

        public int Width { get; set; }
        public int Height { get; set; }

        protected Game()
        {
        }
        public static Game getInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new Game();
                }
            }
            return instance;
        }



        private void Load()
        {
            var rnd = new Random();
            _models = new List<BaseObjectModel>();
            _views = new List<BaseObjectView>();
            _controllers = new List<BaseObjectController>();
            _models.Clear();
            _views.Clear();
            _controllers.Clear();
            

            for (int i = 0; i < 400; i++)
            {
                //Создали модель, скормили её контроллеру и вьюхе
                Thread.Sleep(1); //TODO В качестве hotfix'a в демо версии может и возможно, но такой подход мне не нравится
                int realSize = new Random().Next(3, 7);
                StarModel star = new StarModel(new Point(rnd.Next(0, Width), rnd.Next(0, Height)), new Size(realSize, realSize));
                _models.Add(star);
                _controllers.Add(new StarController(star));
                _views.Add(new StarView(star));

            }

            for (int i = 0; i < (countAsteroid + level - 1); i++)
            {
                //Создали модель, скормили её контроллеру и вьюхе
                Thread.Sleep(1); //TODO В качестве hotfix'a в демо версии может и возможно, но такой подход мне не нравится
                int realSize = new Random().Next(2, 8);
                AsteroidModel asteroid = new AsteroidModel(new Point(rnd.Next(0, Width), rnd.Next(0, Height)), new Size(realSize, realSize));
                _models.Add(asteroid);
                _controllers.Add(new AsteroidController(asteroid));
                _views.Add(new AsteroidView(asteroid));

            }

            ShipModel _ship = new ShipModel(new Point(200, 200), new Point(0, 0), new Size(64, 32));
            _models.Add(_ship);
            _controllers.Add(new ShipController(_ship));
            _views.Add(new ShipView(_ship));

            MedicalModel _medkit = new MedicalModel(new Point(rnd.Next(Width, Width * 2), rnd.Next(0, Height)), new Size(32, 16));
            _models.Add(_medkit);
            _controllers.Add(new MedicalController(_medkit));
            _views.Add(new MedicalView(_medkit));
            
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            DrawForAll();
            ControllersUpdate();
            if(destroyedAsteroids >= (countAsteroid+level-1))
            {
                NextLevel();
            }
        }


        public void Init(Form form)
        {
            // Графическое устройство для вывода графики
            //TODO Вроде и работает но... Cannot convert source type хотя вроде типы одинаковые... ошибка Rider'a или тут реально что-то есть? 
            _form = form;
            Graphics g = _form.CreateGraphics();
            

            // Получаем корректные размеры рабочей области
            Width = _form.ClientRectangle.Width;
            Height = _form.ClientRectangle.Height;
            if(Width > 1000 || Width < 0)
            {
                throw new ArgumentOutOfRangeException("form.ClientRectangle.Width", "Значение должно быть больше чем 0 и меньше чем 1000"); 
            }
            if(Height > 1000 || Height < 0)
            {
                throw new ArgumentOutOfRangeException("form.ClientRectangle.Height", "Значение должно быть больше чем 0 и меньше чем 1000");
            }
            // Связываем буфер в памяти с графическим объектом, чтобы рисовать в буфере
            Buffer = BufferedGraphicsManager.Current.Allocate(g, new Rectangle(0, 0, Width, Height));
            
            Load();

            //Запускаем таймер который будет дергать всё необходимое для обновления
            timer.Start();
            timer.Tick += Timer_Tick;
        }

        public void Finish()
        {
            timer.Stop();
            Game.getInstance().Buffer.Graphics.DrawString("The End", new Font(FontFamily.GenericSansSerif,
            60, FontStyle.Underline), Brushes.White, 200, 100);
            Game.getInstance().Buffer.Render();
        }

        public void NextLevel()
        {
            timer.Stop();
            Game.getInstance().Buffer.Graphics.DrawString($"Level {level} finished!", new Font(FontFamily.GenericSansSerif,
            60, FontStyle.Underline), Brushes.White, 100, 100);
            Game.getInstance().Buffer.Render();
            level++;
            destroyedAsteroids = 0;
            Thread.Sleep(1000);
            _controllers.ForEach(c => c.SelfDestroy());
            _views.ForEach(c => c.SelfDestroy());
            Load();
            timer.Start();
        }

        public void Log(string msg)
        {
            Console.WriteLine($"{msg}");
        }


        private void DrawForAll()
        {
            // Проверяем вывод графики
            Game.getInstance().Buffer.Graphics.Clear(Color.Black);
            Game.getInstance().Buffer.Graphics.DrawImage(Resources.BGLesson1, 0,0, Width, Height);
            ViewsUpdate();
            Buffer.Render();
        }

    }

}