﻿using System.Drawing;

namespace Evladov_AA_Lesson1_HW.Interfaces
{
    public interface ICollision
    {
        bool Collision(ICollision obj);
        Rectangle Rect { get; }
    }
}
