﻿using Evladov_AA_Lesson1_HW.Models;

namespace Evladov_AA_Lesson1_HW.Interfaces
{
    public interface IReaction: ICollision
    {
        void Reaction(ICollision obj);
    }
}
