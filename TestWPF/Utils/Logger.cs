﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWPF.Utils
{
    public class Logger
    {
        Type type;

        public Logger(Type type)
        {
            this.type = type;
        }

        public void Log(string msg)
        {
            Console.WriteLine($"{DateTime.Now.ToString()} {type.Name}\t{msg}");
        }
    }
}
