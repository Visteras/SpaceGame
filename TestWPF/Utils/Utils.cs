﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWPF.Utils
{
    static class Utils
    {
        public static bool CheckExistString(string str, out string result)
        {
            if (str == null || str == "")
            {
                throw new ArgumentException("Аргумент отсутствует");
            } else
            {
                result = str;
                return true;
            }
        }
    }
}
