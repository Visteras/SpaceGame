﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestWPF.ViewModels;

namespace TestWPF.Models
{
    class Department: INotifyPropertyChanged
    {

        private int id;
        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public DataRow Employees
        {
            get {
                Console.WriteLine("Сработало получение списка сотрудников!");
                return VMEmployee.getInstance().dataTable.Rows.Find(this.Id);
            }
        }

        public Department(string name)
        {
            if(Utils.Utils.CheckExistString(name, out string tmp))
            {
                Name = tmp;
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
