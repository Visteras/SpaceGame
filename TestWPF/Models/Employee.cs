﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TestWPF.Models
{
    class Employee: INotifyPropertyChanged
    {

        private int _id;
        private string _family;
        private string _name;
        private string _patrynomic;
        private Department _department;
        public int Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged("Id");
            }
        }
        public string Family {
            get => _family;
            set
            {
                Utils.Utils.CheckExistString(value, out _family);
                OnPropertyChanged("Family");
            }
        }
        public string Name {
            get => _name;
            set
            {
                Utils.Utils.CheckExistString(value, out _name);
                OnPropertyChanged("Name");
            }
        }
        public string Patrynomic {
            get => _patrynomic;
            set {
                _patrynomic = value;
                OnPropertyChanged("Patrynomic");
            }
        }

        public Department Department { get => _department; set
            {
                var tmp = _department;
                _department = value;
                OnPropertyChanged("Department");
                foreach (var dep in VMDepartment.getInstance().Departments.Where(d => d == tmp || d == value))
                {
                    dep.OnPropertyChanged("Employees");
                }
            }
        }

        public Employee(string family, string name, string patrynomic)
        {
            Family = family;
            Name = name;
            Patrynomic = patrynomic;
        }

        public Employee(string family, string name, string patrynomic, Department department): this(family, name, patrynomic)
        {
            if (VMDepartment.getInstance().Departments.Contains(department))
            {
                Department = department;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
