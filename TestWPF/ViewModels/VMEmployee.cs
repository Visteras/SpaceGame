﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestWPF.Models;

namespace TestWPF.ViewModels
{
    class VMEmployee: INotifyPropertyChanged
    {
        private Employee selectedEmployee;
        private static VMEmployee instance;
        private static object syncRoot = new Object();

        public ObservableCollection<Employee> Employees { get; set; }

        public Employee SelectedEmployee
        {
            get => selectedEmployee;
            set
            {
                selectedEmployee = value;
                OnPropertyChanged("SelectedEmployee");
            }

        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                  (addCommand = new RelayCommand(obj =>
                  {
                      //Employee employee = new Employee("Фамилия","Имя","Отчество");
                      //Employees.Add(employee);
                      //SelectedEmployee = employee;
                  }));
            }
        }
        private RelayCommand removeCommand;
        public RelayCommand RemoveCommand
        {
            get
            {
                return removeCommand ??
                    (removeCommand = new RelayCommand(obj =>
                    {
                        Employee employee = obj as Employee;
                        //if (employee != null)
                        //{
                        //    var key = Employees.IndexOf(employee);
                        //    Employees.Remove(employee);
                        //    if (Employees.Count > 0)
                        //    {
                        //        if (key > 0) SelectedEmployee = Employees[key - 1]; else SelectedEmployee = Employees[0];
                        //    }
                        //}
                    }
                    //,
                    //(obj) => Employees.Count > 0
                    ));
            }
        }

        public DataTable dataTable { get; private set; }
        public SqlDataAdapter adapter { get; private set; }

        public static VMEmployee getInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new VMEmployee();
                }
            }
            return instance;
        }

        protected VMEmployee()
        {

            var connectionString = ConfigurationManager.ConnectionStrings["TestWPF.Properties.Settings.Lesson7ConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            adapter = new SqlDataAdapter();
            SqlCommand selectCommand = new SqlCommand("SELECT Id, Family, Name, Patrynomic, DepartmentId FROM Employee", connection);
            adapter.SelectCommand = selectCommand;
            dataTable = new DataTable();
            adapter.Fill(dataTable);



            //Employees = new ObservableCollection<Employee>();
            //var departments = VMDepartment.getInstance();
            //Random random = new Random();
            
            //for (int i = 0; i < 10; i++)
            //{
            //    Employees.Add(new Employee("Фамилия "+i, "Имя " + i, "Отчество "+i, departments.Departments[random.Next(0, departments.Departments.Count)]));
            //}
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
