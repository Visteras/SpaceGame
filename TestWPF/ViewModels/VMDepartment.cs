﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestWPF.ViewModels;
using System.Data.SqlClient;
using System.Data;

namespace TestWPF.Models
{
    class VMDepartment : INotifyPropertyChanged
    {
        private Department selectedDepartment;
        private static VMDepartment instance;
        private static object syncRoot = new Object();
        public ObservableCollection<Department> Departments { get; set; }
        public Department SelectedDepartment
        {
            get => selectedDepartment;
            set
            {
                selectedDepartment = value;
                OnPropertyChanged("SelectedDepartment");
            }

        }
        public DataTable dataTable { get; private set; }
        public DataView viewTable {
            get => dataTable.DefaultView;
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                  (addCommand = new RelayCommand(obj =>
                  {
                      Department department = new Department("Департамент");
                      Departments.Add(department);
                      SelectedDepartment = department;
                  }));
            }
        }
        private RelayCommand removeCommand;
        public RelayCommand RemoveCommand
        {
            get
            {
                return removeCommand ??
                    (removeCommand = new RelayCommand(obj =>
                    {
                        Department department = obj as Department;
                        if (department != null)
                        {
                            var key = Departments.IndexOf(department);
                            Departments.Remove(department);
                            if (Departments.Count > 0)
                            {
                                if (key > 0) SelectedDepartment = Departments[key - 1]; else SelectedDepartment = Departments[0];
                            }

                            
                        }
                    }
                    //,
                    //(obj) => Departments.Count > 0 && VMEmployee.getInstance().Employees.Where(e => e.Department == obj as Department).Count() == 0
                    ));
            }
        }

        public static VMDepartment getInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new VMDepartment();
                }
            }
            return instance;
        }

        protected VMDepartment()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["TestWPF.Properties.Settings.Lesson7ConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand selectCommand = new SqlCommand("SELECT Id, Name FROM Department", connection);
            adapter.SelectCommand = selectCommand;
            dataTable = new DataTable();
            adapter.Fill(dataTable);
            

           
            Departments = new ObservableCollection<Department>();
            //for(int i = 0; i < 10; i++)
            //{
            //    Departments.Add(new Department("Департамент " + i));
            //}
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
