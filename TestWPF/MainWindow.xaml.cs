﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestWPF.Models;
using TestWPF.Windows;

namespace TestWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //List<Employee> employees = new List<Employee>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void openWindowDepartment_Click(object sender, RoutedEventArgs e)
        {
            OpenWindow<WDepartment>();
        }

        private void OpenWindow<T>()
            where T: Window, new()
        {
            bool check = false;
            T obj = null; ;
            foreach (var item in this.OwnedWindows)
            {
                if (item is T)
                {
                    check = true;
                    obj = item as T;
                }
            }

            if (!check)
            {
                T childWindow = new T();
                childWindow.Owner = this;
                childWindow.Show();
            } else
            {
                if (!obj.IsFocused)
                {
                    obj.Focus();
                }
            }
        }

        private void openWindowEmploye_Click(object sender, RoutedEventArgs e)
        {
            OpenWindow<WEmployee>();
        }
    }
}
