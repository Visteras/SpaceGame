﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestWPF.Models;
using TestWPF.ViewModels;

namespace TestWPF.Windows
{
    /// <summary>
    /// Логика взаимодействия для WEmployee.xaml
    /// </summary>
    public partial class WEmployee : Window
    {
        public WEmployee()
        {
            InitializeComponent();

            DataContext = VMEmployee.getInstance();
            listDepartments.ItemsSource = VMDepartment.getInstance().dataTable.DefaultView;
            //listEmployees.DataContext = VMEmployee.getInstance().dataTable.DefaultView;

        }
    }
}
