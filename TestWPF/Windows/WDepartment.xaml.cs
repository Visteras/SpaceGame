﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestWPF.Models;

namespace TestWPF.Windows
{
    /// <summary>
    /// Логика взаимодействия для WDepartment.xaml
    /// </summary>
    public partial class WDepartment : Window
    {
        public WDepartment()
        {
            InitializeComponent();
            DataContext = VMDepartment.getInstance();
            //listDepartments.DataContext = VMDepartment.getInstance().dataTable.DefaultView;
        }
    }
}
