﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 2. Дана коллекция List<T>, требуется подсчитать, сколько раз каждый элемент встречается в данной коллекции:
/// а) для целых чисел;
/// б) * для обобщенной коллекции;
/// в) * используя Linq.
/// </summary>
namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> listA = new List<int>() { 1, 5, 3, 4, 5, 6, 7, 7, 7, 7, 7, 7, 7 };
            foreach (var v in listA.GroupBy(s => s).Select(r => new
            {
                CValue = r.Key,
                CList = r.ToList()
            }).OrderByDescending(r => r.CList.Count))
            {
                Console.WriteLine(
                    $"Значение {v.CValue} встречается {v.CList.Count} раз(а)");
            }
        }
    }
}
